import { contactsClient } from '@lib/contactsClient';
import { useEffect, useState } from 'react';
import { IContact, ListResponse } from 'react-coding-interview-shared/models';

type ContactListState = {
  fetching: boolean;
  hasMore: boolean;
  isLastPage: boolean;
  contacts: ListResponse<IContact>;
  currentPage: number;
};

export type ContactListResult = [ContactListState, () => void];

export function useContactList(pageSize = 20): ContactListResult {
  const [state, setState] = useState<ContactListState>({
    fetching: false,
    hasMore: true,
    isLastPage: false,
    currentPage: 0,
    contacts: { data: [], totalCount: -1 },
  });

  const fetchNextPage = async () => {
    if (!state.fetching && state.hasMore) {
      const nextPage = state.currentPage + 1;
      const totalPages = Math.ceil(state.contacts.totalCount / pageSize);

      const isLastPage = nextPage > totalPages;

      setState((s) => ({ ...s, fetching: true }));

      const resp = await contactsClient.list(nextPage, pageSize);
      const newContacts = [...state.contacts.data, ...resp.data];

      setState({
        fetching: false,
        hasMore: resp.totalCount > 0,
        isLastPage: isLastPage,
        contacts: { data: newContacts, totalCount: resp.totalCount },
        currentPage: nextPage,
      });
    }
  };

  useEffect(() => {
    fetchNextPage();
  }, []);

  return [state, fetchNextPage];
}
