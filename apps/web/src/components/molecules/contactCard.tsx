import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import {useState} from "react";
import {EditField} from "@components/molecules/editField";

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}


export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  const [isEditingName, setIsEditingName] = useState(false);
  const [isEditingEmail, setIsEditingEmail] = useState(false);

  console.log(isEditingName, isEditingEmail);

  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <div onClick={() => setIsEditingName(true)}>
            {
              isEditingName ? <EditField value={name} setIsEditing={setIsEditingName}/> : <Typography variant="subtitle1" lineHeight="1rem">
                {name}
              </Typography>
            }

          </div>

          <div onClick={() => setIsEditingEmail(true)}>
            {
              isEditingEmail ? <EditField value={email} setIsEditing={setIsEditingEmail}/> : <Typography variant="caption" color="text.secondary">
                {email}
              </Typography>
            }

          </div>
        </Box>
      </Box>
    </Card>
  );
};
