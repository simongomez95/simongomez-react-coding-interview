import {useState} from "react";
import {Card} from "@components/atoms";
import {Avatar, Box, Typography} from "@mui/material";
import {IContactCardProps} from "@components/molecules/contactCard";
import {IContact} from "react-coding-interview-shared/_dist/models";
import {SystemStyleObject, Theme} from "@mui/system";

export interface EditFieldProps {
    value: string;
    setIsEditing: (isEditing: boolean) => void;
}
export const EditField: React.FC<EditFieldProps> = ({value, setIsEditing}) => {
    return (
        <div>
            <input value={value}/>
            <button onClick={() => {
                setIsEditing(false)}
            }>
                Ok
            </button>
            <button onClick={() => {setIsEditing(false)}}>
                cancel
            </button>
        </div>
    );
};